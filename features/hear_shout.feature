Feature: Hear Shout

  Shouts have a range up to 1000m

  Background:
    Given Lucy is at 0, 0

  Scenario: In range shout is heard
    And Sean is at 0, 900
    When Sean shouts
    Then Lucy should hear Sean
  
  Rule: One rule to rule them all

    Background:
      Given Lucy is at 0, 0

    Scenario: Out of range shout may not be heard
      And Sean is at 800, 800
      When Sean shouts
      Then Lucy should hear nothing
